###############################################################################
# Script        :   d3dCondaDownloadPkgs.py
# Description   :   Script to download all dependency packages from a Conda
#                   explicit specification file 
#                   (conda list --explicit > spec-file.txt).
# Tested:           Miniconda3 - v4.5.11 (64bit)
# Author        :   David Tufet Palaci
# Email         :   davidtufet@gmail.com
# Version       :   0.1.0
# Last modified :   09/11/2018
###############################################################################


################################################################
# Imports
################################################################
import sys
import os
import argparse
import re
import requests
from datetime import datetime


################################################################
# Constants
################################################################
MY_ARG = ['-f', '--spec-file', True,
            'Input the path+filename of the explicit specification file']
MY_DOWNLOAD_DIR = "downloaded_pkgs_{}"


################################################################
# Functions
################################################################

def check_params():
    # Function to check the passed arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(MY_ARG[0], MY_ARG[1],
                            required=MY_ARG[2], help=MY_ARG[3])
    return parser.parse_args()


def parser_spec_file(my_spec_file):
    # Function to parser the specification file and return a list
    # with all URLs (for all the dependency packages) to downloads

    with open (my_spec_file, "r") as my_file:
                stdout = my_file.read()

    my_pattern = re.compile(r'^(http.*?)$', flags=re.MULTILINE|re.DOTALL)
    list_url = [my_match.group(1) for my_match in my_pattern.finditer(stdout)]
    
    return list_url


def download_packages(list_url):
    # Function to download all the dependency packages

    script_path = os.path.dirname(
                    os.path.realpath(sys.argv[0])).replace('\\', '/')
    my_time = datetime.now().strftime("%Y%m%d-%H%M%S")
    my_download_dir = script_path + "/" + MY_DOWNLOAD_DIR.format(my_time)
    if not os.path.exists(my_download_dir): os.makedirs(my_download_dir)

    for my_url in list_url:
        print ('> Downloading: {}...'.format(my_url))
        my_download_file = my_download_dir + "/" + my_url.split("/")[-1]
        ret = requests.get(my_url)        
        if ret.status_code == 200:
            with open(my_download_file, 'wb') as f_handle:
                f_handle.write(ret.content)
            print ('[ OK ] {}'.format(my_download_file))
        else:
            print("[ ERROR ] {}".format(ret.__str__()))


def main():
    # Function with the main
    
    my_args = check_params()
    list_url = parser_spec_file(my_args.spec_file)
    download_packages(list_url)



################################################################
# Main
################################################################

if __name__ == '__main__':
    main()

